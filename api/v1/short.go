package v1

import (
	"dentd/types"

	r "gopkg.in/rethinkdb/rethinkdb-go.v5"
)

var (
	resp    = types.NewResponse
	errresp = types.NewErrResponse
	f       = r.Row.Field
)
