package v1

import (
	"context"
	"dentd/schema"
	"dentd/types"
	"dentd/vars"
	"errors"
	"fmt"
)

// MeRequest holds the data for the me endpoint
type MeRequest struct {
}

// Do implements the RouteHanlder interface
func (m *MeRequest) Do(ctx context.Context) *types.Response {
	token, ok := ctx.Value(vars.CDToken).(*schema.Token)
	if !ok {
		fmt.Printf("Token type: %T\n", ctx.Value(vars.CDToken))
		return errresp(502, errors.New("Error while authenticating the request"))
	}
	return resp(200, token.User, nil)
}
