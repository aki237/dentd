package v1

import (
	"context"
	"dentd/schema"
	"dentd/store"
	"dentd/types"
	"errors"
	"log"
	"regexp"
	"time"

	r "gopkg.in/rethinkdb/rethinkdb-go.v5"
)

// SignupRequest holds the data for the signup endpoint
type SignupRequest struct {
	Email    string `json:"email"`
	ID       string `json:"id"`
	Password string `json:"password"`
}

var (
	emailRe    = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
	usernameRe = regexp.MustCompile("^[a-z][a-z0-9_]*$")
)

// Do implements the RouteHanlder interface
func (sr *SignupRequest) Do(_ context.Context) *types.Response {
	if status, err := sr.Validate(); err != nil {
		return errresp(status, err)
	}

	u := schema.User{
		ID:         sr.ID,
		Email:      sr.Email,
		Password:   hash(sr.Password),
		DateJoined: time.Now(),
		IsVerified: true,
		Deleted:    false,
	}

	_, err := store.Users.Insert(&u).RunWrite(store.Session)
	if err != nil {
		log.Println("RethinkDB filter: ", err)
		return errresp(502, err)
	}

	return resp(200, types.JO{
		"ok":      true,
		"message": "Account created successfully",
	}, nil)
}

// Validate validates the incoming data
func (sr *SignupRequest) Validate() (int, error) {
	switch true {
	case !emailRe.MatchString(sr.Email):
		return 403, errors.New("Invalid email address passed")
	case !usernameRe.MatchString(sr.ID):
		return 403, errors.New("Invalid username passed")
	case len(sr.Password) < 8:
		return 403, errors.New("Password length should be greater than 8")
	}

	count, err := store.Users.Count(
		r.Or(
			f("email").Eq(sr.Email),
			f("id").Eq(sr.ID),
		),
	)
	if err != nil {
		log.Println("RethinkDB filter: ", err)
		return 502, err
	}
	if count != 0 {
		return 403, errors.New("An account with same email exists")
	}

	return 200, nil
}
