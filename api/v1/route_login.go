package v1

import (
	"context"
	"dentd/schema"
	"dentd/store"
	"dentd/types"
	"errors"
	"log"

	r "gopkg.in/rethinkdb/rethinkdb-go.v5"
)

// LoginRequest holds the data for the login endpoint
type LoginRequest struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}

// Do implements the RouteHanlder interface
func (l *LoginRequest) Do(_ context.Context) *types.Response {
	u := schema.User{}
	err := store.Users.FindOne(
		f("email").Eq(l.Login).Or(
			f("id").Eq(l.Login),
		).And(
			f("password").Eq(hash(l.Password)),
		), &u,
	)
	if err != nil {
		if err == r.ErrEmptyResult {
			return errresp(400, errors.New("Invalid credentials"))
		}
		log.Println("RethinkDB filter: ", err)
		return errresp(502, err)
	}

	t := schema.NewToken(&u)

	tstr, err := t.GetTokenString()
	if err != nil {
		log.Println("JWT error: ", err)
		return errresp(502, err)
	}

	return resp(200, types.JO{
		"token": tstr,
	}, nil)
}
