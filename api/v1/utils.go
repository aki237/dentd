package v1

import (
	"crypto/sha512"
	"encoding/hex"
)

func hash(in string) string {
	in += "dentd_secret"
	h := sha512.New()
	h.Write([]byte(in))
	return hex.EncodeToString(h.Sum(nil))
}
