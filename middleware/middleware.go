package middleware

import (
	"log"
	"net/http"
)

// Middlewares is a list of Middleware functions
type Middlewares []Middleware

// To converts Middlewares to router compatible middleware functions
func (m Middlewares) To() []func(http.Handler) http.Handler {
	nexts := make([]func(http.Handler) http.Handler, 0)
	for _, val := range m {
		nexts = append(nexts, val.Next)
	}

	return nexts
}

// Middleware defines a middleware function type
type Middleware func(w http.ResponseWriter, r *http.Request) error

// Next implements the go-chi middleware function signature
func (ml Middleware) Next(next http.Handler) http.Handler {
	return &wrap{ml: ml, next: next}
}

type wrap struct {
	ml   Middleware
	next http.Handler
}

func (wr *wrap) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if err := wr.ml(w, r); err != nil {
		log.Println("[DEBUG] Middleware returned error: ", err)
		return
	}

	if wr.next == nil {
		return
	}
	wr.next.ServeHTTP(w, r)
}
