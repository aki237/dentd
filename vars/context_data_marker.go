package vars

// ContextData is a special type which is used to get the values
// out of the request context
type ContextData int

// Constants of the ContextData
const (
	CDToken ContextData = iota
)
