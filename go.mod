module dentd

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.1-0.20190620180102-5e25c22bd5d6+incompatible
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/google/uuid v1.1.1
	gopkg.in/rethinkdb/rethinkdb-go.v5 v5.0.1
)
