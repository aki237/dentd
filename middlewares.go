package main

import (
	"context"
	"dentd/schema"
	"dentd/store"
	"dentd/types"
	"dentd/vars"
	"errors"
	"log"
	"net/http"
	"strings"

	"gopkg.in/rethinkdb/rethinkdb-go.v5"
)

func degub(_ http.ResponseWriter, r *http.Request) error {
	log.Printf("%s %s\n", r.Method, r.URL.Path)

	return nil
}

var errs = "Unauthorized"

func auth(w http.ResponseWriter, r *http.Request) error {
	key := strings.TrimSpace((strings.TrimPrefix(r.Header.Get("Authorization"), "JWT ")))

	if key == "" {
		types.NewResponse(401, nil, types.JO{
			"error": errs,
		}).Commit(w)
		return errors.New(errs)
	}

	t := schema.NewToken(nil)
	err := t.From(key)
	if err != nil {
		types.NewResponse(401, nil, types.JO{
			"error": err.Error(),
		}).Commit(w)
		return err
	}

	f := rethinkdb.Row.Field

	count, err := store.Users.Count(
		rethinkdb.And(
			f("email").Eq(t.User.Email),
			f("deleted").Eq(false),
		),
	)
	if err != nil {
		types.NewResponse(502, nil, types.JO{
			"error": err.Error(),
		}).Commit(w)
		return err
	}

	if count == 0 {
		types.NewResponse(401, nil, types.JO{
			"error": errs,
		}).Commit(w)
		return errors.New(errs)
	}

	*r = *r.WithContext(context.WithValue(r.Context(), vars.CDToken, t))
	return nil
}
