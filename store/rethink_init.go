package store

import "gopkg.in/rethinkdb/rethinkdb-go.v5"

// All the needed database variables
var (
	Session *rethinkdb.Session
	Users   = NewTable("dentd", "users")
)
