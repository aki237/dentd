package store

import (
	"gopkg.in/rethinkdb/rethinkdb-go.v5"
)

// Table denotes a simple rethinkDB table interface
type Table struct {
	rethinkdb.Term
}

// NewTable creates a new table object
func NewTable(db, table string) *Table {
	return &Table{
		Term: rethinkdb.DB(db).Table(table),
	}
}

// FindOne finds a single record for the passed query
func (t *Table) FindOne(query interface{}, v interface{}) error {
	c, err := t.Term.Filter(query).Run(Session)
	if err != nil {
		return err
	}
	defer c.Close()

	return c.One(v)
}

// Count gets the count of all the rows for a passed query
func (t *Table) Count(query interface{}) (int64, error) {
	count := int64(0)
	c, err := t.Term.Count(query).Run(Session)
	if err != nil {
		return -1, err
	}

	defer c.Close()

	return count, c.One(&count)
}
