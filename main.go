package main

import (
	"dentd/store"
	"dentd/types"
	"log"
	"net/http"

	"github.com/go-chi/chi"

	r "gopkg.in/rethinkdb/rethinkdb-go.v5"
)

func main() {
	log.Println("Starting the dentd server")
	router := chi.NewRouter()

	var err error

	store.Session, err = r.Connect(r.ConnectOpts{
		Address:    "127.0.0.1:28015",
		Database:   "dentd",
		InitialCap: 10,
		MaxOpen:    10,
	})
	if err != nil {
		log.Fatalln(err)
	}

	w := types.NewWrapper

	for _, rg := range routegroups {
		router.With(rg.middlewares.To()...).Group(func(rx chi.Router) {
			for route, methods := range rg.routes {
				for method, rhf := range methods {
					rx.Method(method, route, w(rhf))
				}
			}
		})
	}
	http.ListenAndServe(":8909", router)
}
