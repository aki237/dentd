package main

import (
	v1 "dentd/api/v1"
	"dentd/middleware"
	"dentd/types"
)

type routeGroup struct {
	middlewares middleware.Middlewares
	routes      map[string]map[string]types.RHFactory
}

var routegroups = []routeGroup{
	{middlewares: nil, routes: unauthorizedRoutes},
	{
		middlewares: middleware.Middlewares{
			degub,
			auth,
		},
		routes: authorizedRoutes,
	},
}

var unauthorizedRoutes = map[string]map[string]types.RHFactory{
	"/v1/signup": {
		"POST": func() types.RouteHandler { return new(v1.SignupRequest) },
	},
	"/v1/login": {
		"POST": func() types.RouteHandler { return new(v1.LoginRequest) },
	},
}

var authorizedRoutes = map[string]map[string]types.RHFactory{
	"/v1/me": {
		"GET": func() types.RouteHandler { return new(v1.MeRequest) },
	},
}
