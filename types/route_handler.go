package types

import (
	"context"
	"encoding/json"
	"io"
	"net/http"
)

// RHFactory creates a new RouteHandler instances
// of a given implementation
type RHFactory func() RouteHandler

// RouteHandler interface defines how a request payload
// object should be handled for a particular request parameters
type RouteHandler interface {
	Do(ctx context.Context) *Response
}

// Response is the response returned by the route handling struct methods
type Response struct {
	ResponseData

	StatusCode int
	Header     map[string]string

	EncodeFunc func(w io.Writer, v interface{}) error
}

// ResponseData contains the JSON representaion of the return data to be sent
type ResponseData struct {
	Data interface{} `json:"data,omitempty"`
	Meta interface{} `json:"meta,omitempty"`
}

// NewResponse create a new response object
func NewResponse(statuscode int, data, meta interface{}) *Response {
	return &Response{
		StatusCode: statuscode,
		ResponseData: ResponseData{
			Data: data,
			Meta: meta,
		},

		EncodeFunc: defaultJSONEncodeFunc,
	}
}

// NewErrResponse creates a new response object with just the error configured
func NewErrResponse(statuscode int, err error) *Response {
	return NewResponse(statuscode, nil, JO{
		"error": err.Error(),
	})
}

// Commit comits the response to the response writer
func (r Response) Commit(w http.ResponseWriter) {
	h := w.Header()
	for key, value := range r.Header {
		h.Set(key, value)
	}

	w.WriteHeader(r.StatusCode)

	if r.EncodeFunc == nil {
		r.EncodeFunc = defaultJSONEncodeFunc
	}

	r.EncodeFunc(w, r.ResponseData)
}

func defaultJSONEncodeFunc(w io.Writer, v interface{}) error {
	return json.NewEncoder(w).Encode(v)
}
