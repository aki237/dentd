package types

// JO indicates a simple JSON object
type JO map[string]interface{}

// JA indicates a simple JSON array
type JA []interface{}
