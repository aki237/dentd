package types

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"reflect"
	"strconv"
	"strings"

	"github.com/go-chi/chi"
)

// Wrapper wraps an arbitraty type and proxies the
// appropriate request and response to the struct
// reciever function and the chi router respectively.
type Wrapper struct {
	DecoderFunc  func(w io.Reader, v interface{}) error
	PayloadMaker RHFactory
}

func defaultJSONDecodeFunc(rd io.Reader, v interface{}) error {
	return json.NewDecoder(rd).Decode(v)
}

// ServeHTTP method implements the http.Hanlder interface for
// the Wrap struct
func (wr *Wrapper) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if wr.DecoderFunc == nil {
		wr.DecoderFunc = defaultJSONDecodeFunc
	}

	if wr.PayloadMaker == nil {
		NewResponse(404, nil, nil).Commit(w)
		return
	}
	payload := wr.PayloadMaker()

	if strings.HasPrefix(r.Header.Get("Content-Type"), "application/json") {
		if err := wr.DecoderFunc(r.Body, payload); err != nil {
			NewResponse(403, nil, JO{"error": err.Error()}).Commit(w)
			return
		}
	}

	rctx := chi.RouteContext(r.Context())

	urlparams := make(map[string][]string, 0)
	queryparams := make(map[string][]string, 0)

	for i, v := range rctx.URLParams.Keys {
		urlparams[v] = []string{rctx.URLParams.Values[i]}
	}

	queryparams = r.URL.Query()

	setParams(payload, map[string]map[string][]string{
		"url":    urlparams,
		"query":  queryparams,
		"header": r.Header,
	})

	safe(payload.Do(r.Context())).Commit(w)
}

func safe(rh *Response) *Response {
	if rh != nil {
		return rh
	}

	return NewResponse(200, nil, nil)
}

// SetURLParams is used to set the URL parameters in the payload struct.
func setParams(v interface{}, params map[string]map[string][]string) {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("[WARN] Recovered: ", r)
		}
	}()
	t := reflect.ValueOf(v)
	x := t.Type().Elem()

	for i := 0; i < x.NumField(); i++ {
		if !t.Elem().Field(i).CanSet() {
			continue
		}
		field := x.Field(i)

		for key, valparams := range params {
			tag := field.Tag.Get(key)
			tagVal := valparams[tag]
			if tag == "" || strings.HasPrefix(tag, "-") || len(tagVal) == 0 {
				continue
			}

			if t.Elem().Field(i).Kind() != reflect.Array && t.Elem().Field(i).Kind() != reflect.Slice {
				setValue(tagVal[0], t.Elem().Field(i))
				continue
			}
			t.Elem().Field(i).Set(reflect.ValueOf(tagVal))
		}
	}
}

func setValue(k string, v reflect.Value) {
	switch v.Kind() {
	case reflect.Bool:
		a, _ := strconv.ParseBool(k)
		v.SetBool(a)
	case reflect.Int, reflect.Int64:
		a, _ := strconv.ParseInt(k, 10, 64)
		v.SetInt(a)
	case reflect.Int8:
		a, _ := strconv.ParseInt(k, 10, 8)
		v.SetInt(a)
	case reflect.Int16:
		a, _ := strconv.ParseInt(k, 10, 16)
		v.SetInt(a)
	case reflect.Int32:
		a, _ := strconv.ParseInt(k, 10, 32)
		v.SetInt(a)
	case reflect.Uint, reflect.Uint64:
		a, _ := strconv.ParseUint(k, 10, 64)
		v.SetUint(a)
	case reflect.Uint8:
		a, _ := strconv.ParseUint(k, 10, 8)
		v.SetUint(a)
	case reflect.Uint16:
		a, _ := strconv.ParseUint(k, 10, 16)
		v.SetUint(a)
	case reflect.Uint32:
		a, _ := strconv.ParseUint(k, 10, 32)
		v.SetUint(a)
	case reflect.Float32:
		a, _ := strconv.ParseFloat(k, 32)
		v.SetFloat(a)
	case reflect.Float64:
		a, _ := strconv.ParseFloat(k, 64)
		v.SetFloat(a)
	case reflect.String:
		v.SetString(k)
	default:
		fmt.Printf("[WARN] Unable to set value %s\n", k)
	}
}

// Wrap creates a new http.HanldeFunc by wrapping a new RouteHandler
func Wrap(v RHFactory) func(w http.ResponseWriter, r *http.Request) {
	return NewWrapper(v).ServeHTTP
}

// NewWrapper creates a new wrapper object for the passed RHFactory
func NewWrapper(v RHFactory) *Wrapper {
	return &Wrapper{
		DecoderFunc:  defaultJSONDecodeFunc,
		PayloadMaker: v,
	}
}
