package schema

import (
	"dentd/vars"
	"encoding/hex"
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
)

// Token contains all the details of a single user's token
type Token struct {
	User      *User     `json:"data,omitempty"`
	Expires   time.Time `json:"expires"`
	UniquePad string    `json:"@tid"`
	Issuer    string    `json:"issuer"`
}

// NewToken creates a new token for a passed user.
func NewToken(u *User) *Token {
	return &Token{
		User:      u,
		Expires:   time.Now().Add(time.Duration(24*15) * time.Hour),
		UniquePad: uuid.New().String(),
		Issuer:    "dentd_provision",
	}
}

// From forms the token struct from the passed token string
func (t *Token) From(tokenRaw string) error {
	tokenBytes, err := hex.DecodeString(tokenRaw)
	if err != nil {
		return err
	}

	_, err = jwt.ParseWithClaims(string(tokenBytes), t, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return vars.SigningSecret, nil
	})

	return err
}

// Valid implements the jwt.Claim interface
func (t *Token) Valid() error {
	if !t.IsValid() {
		return errors.New("Token expired")
	}

	if strings.TrimSpace(t.UniquePad) == "" || strings.TrimSpace(t.Issuer) == "" {
		return errors.New("Malformed token addends")
	}

	if t.User == nil {
		return errors.New("Request entity profile is nil")
	}

	return nil
}

// IsValid returns whether the token is still valid
func (t Token) IsValid() bool {
	return t.Expires.After(time.Now())
}

// GetTokenString returns the hex representation of JWT
func (t *Token) GetTokenString() (tokenRaw string, err error) {
	tokenRaw, err = jwt.NewWithClaims(jwt.SigningMethodHS256, t).SignedString(vars.SigningSecret)
	if err != nil {
		return
	}

	tokenRaw = hex.EncodeToString([]byte(tokenRaw))
	return
}
