package schema

import "time"

// User struct denotes a single record of the users table
type User struct {
	ID         string    `rethinkdb:"id" json:"id"`
	Email      string    `rethinkdb:"email" json:"email"`
	IsVerified bool      `rethinkdb:"is_verified" json:"-"`
	Deleted    bool      `rethinkdb:"deleted" json:"-"`
	DateJoined time.Time `rethinkdb:"date_joined" json:"date_joined"`
	Password   string    `rethinkdb:"password" json:"-"`
}
