#!/usr/bin/env python3

import random
from itertools import repeat
bytestring = ""
for i in repeat(None, 40):
    bytestring += str(random.randrange(0, 255, 1)) + ", "

bytestring = bytestring.strip()

print (f"package vars\n\nvar SigningSecret = []byte {{\n\t{bytestring}\n}}")